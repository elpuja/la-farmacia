/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package logica;

import persistencia.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class Producto {
    private String nombre;
    private String id;
    private double temperatura;
    private double valorBase;

    public Producto(String nombre, String id, double temperatura, double valorBase) {
        this.nombre = nombre;
        this.id = id;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public Producto() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "{" + "nombre=" + nombre + ", id=" + id + ", temperatura=" + temperatura + ", valorBase=" + valorBase + '}';
    }
    
    // Read
    public List<Producto> listarProductos() throws ClassNotFoundException{
        List<Producto> listaProductos = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Producto p;
            while (rs.next()) {
                p = new Producto();
                p.setNombre(rs.getString("nombre"));
                p.setId(rs.getString("id"));
                p.setTemperatura(rs.getDouble("temperatura"));
                p.setValorBase(rs.getDouble("valorBase"));
                listaProductos.add(p);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }

    // Crear
    public boolean guardarProducto() throws ClassNotFoundException {
        ConexionBD conexion = new ConexionBD();
        String sql = "INSERT INTO productos(id, nombre, temperatura,valorBase)"
                + "VALUES('" + this.nombre + "'," + this.id + ",'" + this.temperatura + "'," + this.valorBase + ");";
        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    // updata
    public boolean actualizarProducto() throws ClassNotFoundException {
        ConexionBD conexion = new ConexionBD();
        String sql = "UPDATE productos SET nombre='"
                + this.nombre + "',id=" + this.id
                + ",temperatura='" + this.temperatura + "',valorBase="
                + this.valorBase + " WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    // Delete
    public boolean eliminarProducto() throws ClassNotFoundException {
        ConexionBD conexion = new ConexionBD();
        String sql = "DELETE FROM productos WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
}

